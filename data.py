from keras.preprocessing.image import ImageDataGenerator
import numpy as np 
import os
import glob
import numpy as np
import pandas as pd
from scipy.ndimage.interpolation import map_coordinates
from scipy.ndimage.filters import gaussian_filter
#from libtiff import TIFF

class myAugmentation(object):
	
	"""
	A class used to augmentate image
	Firstly, read train image and label seperately, and then merge them together for the next process
	Secondly, use keras preprocessing to augmentate image
	Finally, seperate augmentated image apart into train image and label
	"""

	def __init__(self, train_path="data/train/image/", label_path="data/train/label/", merge_path="merge", aug_merge_path="aug_merge", aug_train_path="data/aug_train/image/", aug_label_path="data/aug_train/label/"):
		
		"""
		Using glob to get all .img_type form path
		"""

		self.train_path = train_path
		self.label_path = label_path
		self.merge_path = merge_path
		self.aug_train_path = aug_train_path
		self.aug_label_path = aug_label_path
		self.datagen = ImageDataGenerator(rotation_range=90,width_shift_range=0.1,height_shift_range=0.1,
                             shear_range=0.4,zoom_range=[.2,1.2],
                             vertical_flip=True,horizontal_flip=True,fill_mode='reflect')

	def Augmentation(self,numnew,start=0,elastic=0):
		num=np.array([2,3,4,6,7,8,9])
		size=np.array([10,8,.578,.756,.778,.770,.792])
		size/=sum(size)
		inp_size=256

		"""
		Start augmentation.....
		"""
		for i in range(numnew):
			z=np.random.choice(num,p=size)
			imt1=np.load('data/train/image/'+str(z)+'.npy')
			if (z==2 or z==4) and (np.random.rand()>.5):
				imt2=np.load('data/train/label/'+str(z)+'s.npy')
			else:
				imt2=np.load('data/train/label/'+str(z)+'t.npy')
			[m,n]=imt1.shape
			if elastic:
				imag=np.zeros((m,n,2))
				imag[:,:,0]=imt1
				imag[:,:,1]=imt2
				imag_t = self.elastic_transform(imag, imag.shape[1] * 2, imag.shape[1] * 0.05, imag.shape[1] * 0.08)
				im_t = imag_t[:,:,0]
				im_mask_t = imag_t[:,:,1]
			else:
				imag=np.zeros((m,n,3))
				imag[:,:,0]=imt1
				imag[:,:,2]=imt2
				imag = imag.reshape((1,) + imag.shape)
				iterator=self.datagen.flow(imag)
				imag_t=iterator.next()
				im_t = imag_t[0,:,:,0]
				im_mask_t = imag_t[0,:,:,2]

			if m>=inp_size:
				bottom=np.random.randint(m-inp_size+1)
				im_t=im_t[bottom:bottom+inp_size,:]
				im_mask_t=im_mask_t[bottom:bottom+inp_size,:]
			else:
				M=(inp_size-m)//2
				re=(inp_size-m)%2
				im_t=np.lib.pad(im_t,((M, M+re),(0,0)),'reflect')
				im_mask_t=np.lib.pad(im_mask_t,((M, M+re),(0,0)),'reflect')
			if n>=inp_size:
				left=np.random.randint(n-inp_size+1)
				im_t=im_t[:,left:left+inp_size]
				im_mask_t=im_mask_t[:,left:left+inp_size]
			else:
				N=(inp_size-n)//2
				re=(inp_size-n)%2
				im_t=np.lib.pad(im_t,((0,0),(N, N+re)),'reflect')
				im_mask_t=np.lib.pad(im_mask_t,((0,0),(N, N+re)),'reflect')

			np.save('data/aug_train/image/'+str(start+i)+'.npy',(im_t*255).astype(np.uint8))
			np.save('data/aug_train/label/'+str(start+i)+'t.npy',(im_mask_t>.5).astype(np.uint8))



	'''
	# Function to distort image
	def elastic_transform(self,image, alpha, sigma, alpha_affine, random_state=None):
	    """Elastic deformation of images as described in [Simard2003]_ (with modifications).
	    .. [Simard2003] Simard, Steinkraus and Platt, "Best Practices for
	         Convolutional Neural Networks applied to Visual Document Analysis", in
	         Proc. of the International Conference on Document Analysis and
	         Recognition, 2003.

	     Based on https://gist.github.com/erniejunior/601cdf56d2b424757de5
	    """
	    if random_state is None:
	        random_state = np.random.RandomState(None)

	    shape = image.shape
	    shape_size = shape[:2]
	    
	    # Random affine
	    center_square = np.float32(shape_size) // 2
	    square_size = min(shape_size) // 3
	    pts1 = np.float32([center_square + square_size, [center_square[0]+square_size, center_square[1]-square_size], center_square - square_size])
	    pts2 = pts1 + random_state.uniform(-alpha_affine, alpha_affine, size=pts1.shape).astype(np.float32)
	    M = cv2.getAffineTransform(pts1, pts2)
	    image = cv2.warpAffine(image, M, shape_size[::-1], borderMode=cv2.BORDER_REFLECT_101)

	    dx = gaussian_filter((random_state.rand(*shape) * 2 - 1), sigma) * alpha
	    dy = gaussian_filter((random_state.rand(*shape) * 2 - 1), sigma) * alpha
	    dz = np.zeros_like(dx)

	    x, y, z = np.meshgrid(np.arange(shape[1]), np.arange(shape[0]), np.arange(shape[2]))
	    indices = np.reshape(y+dy, (-1, 1)), np.reshape(x+dx, (-1, 1)), np.reshape(z, (-1, 1))

	    return map_coordinates(image, indices, order=1, mode='reflect').reshape(shape)
	    '''


class dataProcess(object):

	def __init__(self, out_rows, out_cols,tar_rows, tar_cols, data_path = "data/aug_train/image/", label_path = "data/aug_train/label/", test_path = "data/test/", npy_path = "../npydata", img_type = "npy"):

		"""
		
		"""

		self.out_rows = out_rows
		self.out_cols = out_cols
		self.tar_rows = tar_rows
		self.tar_cols = tar_cols
		self.data_path = data_path
		self.label_path = label_path
		self.img_type = img_type
		self.test_path = test_path
		self.npy_path = npy_path

	def create_train_data(self):
		print('-'*30)
		print('Creating training images...')
		print('-'*30)
		imgs = glob.glob(self.data_path+"/*."+self.img_type)
		N=len(imgs)
		imgdatas = np.ndarray((len(imgs),self.out_rows,self.out_cols,1), dtype=np.uint8)
		imglabels = np.ndarray((len(imgs),self.tar_rows,self.tar_cols,1), dtype=np.uint8)
		begin_row=int((self.out_rows-self.tar_rows)/2)
		begin_col=int((self.out_cols-self.tar_cols)/2)
		for i in range(N):
			img=np.load('data/aug_train/image/'+str(i)+'.npy')
			label=np.load('data/aug_train/label/'+str(i)+'t.npy')
			#img = cv2.imread(self.data_path + "/" + midname,cv2.IMREAD_GRAYSCALE)
			#label = cv2.imread(self.label_path + "/" + midname,cv2.IMREAD_GRAYSCALE)
			#img = np.array([img])
			#label = np.array([label])
			imgdatas[i,:,:,0] = img
			imglabels[i,:,:,0] = label[begin_row:begin_row+self.tar_rows,begin_col:begin_col+self.tar_cols]
		imgdatas = imgdatas.astype('float32')
		imglabels = imglabels.astype('float32')
		imgdatas /= 255
		imglabels[imglabels > 0.5] = 1
		imglabels[imglabels <= 0.5] = 0
		return imgdatas,imglabels

	def create_deform_test_data(self,numnew=1000):
		print('-'*30)
		print('Creating some images...')
		print('-'*30)
		num=np.array([1,5])
		inp_size=256
		imgdatas = np.ndarray((numnew,self.out_rows,self.out_cols,1))
		for i in range(numnew):
			z=np.random.choice(num)
			imt1=np.load('data/test/'+str(z)+'.npy')
			[m,n]=imt1.shape
			im_t = imt1
			if m>=inp_size:
				bottom=np.random.randint(m-inp_size+1)
				im_t=im_t[bottom:bottom+inp_size,:]
			else:
				M=(inp_size-m)//2
				re=(inp_size-m)%2
				im_t=np.lib.pad(im_t,((M, M+re),(0,0)),'reflect')
			if n>=inp_size:
				left=np.random.randint(n-inp_size+1)
				im_t=im_t[:,left:left+inp_size]
			else:
				N=(inp_size-n)//2
				re=(inp_size-n)%2
				im_t=np.lib.pad(im_t,((0,0),(N, N+re)),'reflect')
			imgdatas[i,:,:,0]=im_t
		return imgdatas

	def create_label_test_data(self,numnew=1000):
		num=np.array([2,3,4,6,7,8,9])
		size=np.array([10,8,.578,.756,.778,.770,.792])
		size/=sum(size)
		inp_size=256

		"""
		Start augmentation.....
		"""
		imgdatas = np.ndarray((numnew,inp_size,inp_size,1))
		for i in range(numnew):
			z=np.random.choice(num,p=size)
			if (z==2 or z==4) and (np.random.rand()>.5):
				imt2=np.load('data/train/label/'+str(z)+'s.npy')
			else:
				imt2=np.load('data/train/label/'+str(z)+'t.npy')
			[m,n]=imt2.shape

			if m>=inp_size:
				bottom=np.random.randint(m-inp_size+1)
				imt2=imt2[bottom:bottom+inp_size,:]
			else:
				M=(inp_size-m)//2
				re=(inp_size-m)%2
				imt2=np.lib.pad(imt2,((M, M+re),(0,0)),'reflect')
			if n>=inp_size:
				left=np.random.randint(n-inp_size+1)
				imt2=imt2[:,left:left+inp_size]
			else:
				N=(inp_size-n)//2
				re=(inp_size-n)%2
				imt2=np.lib.pad(imt2,((0,0),(N, N+re)),'reflect')
			imgdatas[i,:,:,0]=imt2
		return imgdatas

	def create_img_test_data(self,numnew=1000):
		num=np.array([2,3,4,6,7,8,9])
		size=np.array([10,8,.578,.756,.778,.770,.792])
		size/=sum(size)
		inp_size=256

		"""
		Start augmentation.....
		"""
		imgdatas = np.ndarray((numnew,inp_size,inp_size,1))
		for i in range(numnew):
			z=np.random.choice(num,p=size)
			imt2=np.load('data/train/image/'+str(z)+'.npy')
			[m,n]=imt2.shape

			if m>=inp_size:
				bottom=np.random.randint(m-inp_size+1)
				imt2=imt2[bottom:bottom+inp_size,:]
			else:
				M=(inp_size-m)//2
				re=(inp_size-m)%2
				imt2=np.lib.pad(imt2,((M, M+re),(0,0)),'reflect')
			if n>=inp_size:
				left=np.random.randint(n-inp_size+1)
				imt2=imt2[:,left:left+inp_size]
			else:
				N=(inp_size-n)//2
				re=(inp_size-n)%2
				imt2=np.lib.pad(imt2,((0,0),(N, N+re)),'reflect')
			imgdatas[i,:,:,0]=imt2
		return imgdatas

	def create_test_data(self,num):
		z = 0
		print('-'*30)
		print('Creating test images...')
		print('-'*30)
		
		img=np.load(self.test_path+str(num)+'.npy')
		[m,n]=img.shape
		overlap=10
		numr=m//(self.tar_rows-overlap)+1
		numc=n//(self.tar_cols-overlap)+1

		imgdatas = np.ndarray((numr*numc,self.out_rows,self.out_cols,1), dtype=np.float32)

		rows=int((self.out_rows-self.tar_rows)/2)
		cols=int((self.out_cols-self.tar_cols)/2)
		img=np.lib.pad(img,((rows,self.out_rows),(cols, self.out_cols)),'reflect')
		for i in range(numr):
			for j in range(numc):
				colstart=j*(self.tar_cols-overlap)
				rowstart=i*(self.tar_rows-overlap)
				temp=img[rowstart:rowstart+self.out_rows,colstart:colstart+self.out_cols]
			#img = cv2.imread(self.test_path + "/" + midname,cv2.IMREAD_GRAYSCALE)
			#img = np.array([img])
				imgdatas[z,:,:,0] = temp
				z += 1
		return imgdatas


if __name__ == "__main__":
	pass
	#aug.splitMerge()
	#aug.splitTransform()
	#mydata = dataProcess(512,512)
	#mydata.create_train_data()
	#mydata.create_test_data()
	#imgs_train,imgs_mask_train = mydata.load_train_data()
	#print imgs_train.shape,imgs_mask_train.shape
