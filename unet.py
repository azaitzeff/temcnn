import os 
#os.environ["CUDA_VISIBLE_DEVICES"] = "0"
import numpy as np
from keras.preprocessing.image import array_to_img
from keras.models import *
from keras.layers import Input, concatenate, Conv2D,Conv2DTranspose, MaxPooling2D, UpSampling2D, Dropout, Cropping2D, Reshape, BatchNormalization,average
from keras.layers.advanced_activations import LeakyReLU
from keras.optimizers import *
from keras.layers.core import Flatten, Dense, Dropout, Activation, Lambda, Reshape
from keras.callbacks import ModelCheckpoint, LearningRateScheduler, ReduceLROnPlateau
from keras import backend as keras
from keras import losses
from data import *
import sys
import logging


class myUnet(object):

	def __init__(self, img_rows = 256, img_cols = 256,lab_rows=256,lab_cols=256,patch_size=256):

		self.img_rows = img_rows
		self.img_cols = img_cols
		self.lab_rows = lab_rows
		self.lab_cols = lab_cols
		self.patch_size=patch_size

	def load_data(self):

		mydata = dataProcess(self.img_rows, self.img_cols,self.lab_rows, self.lab_cols)
		imgs_train, imgs_mask_train = mydata.create_train_data()
		#imgs_test = mydata.load_test_data()
		return imgs_train, imgs_mask_train


	def load_test(self,num):

		mydata = dataProcess(self.img_rows, self.img_cols,self.lab_rows, self.lab_cols)
		imgs_test = mydata.create_test_data(num)
		test_path = "data/test/"
		image_orig=np.load(test_path+str(num)+'.npy')
		#imgs_test = mydata.load_test_data()
		return imgs_test,image_orig

	def load_deform_test(self,num):

		mydata = dataProcess(self.img_rows, self.img_cols,self.lab_rows, self.lab_cols)
		imgs_test = mydata.create_img_test_data(num)
		#imgs_test = mydata.load_test_data()
		return imgs_test

	def load_deform_data(self):

		deformed=np.load('data/samples/deformed.npy')
		matlab=np.load('data/samples/matlab.npy')
		N1=deformed.shape[0]
		N2=matlab.shape[0]
		N3=N1-N2
		imgs_test=np.zeros(((N1+N2+N3),self.lab_rows,self.lab_cols,1))
		imgs_labels=np.zeros(((N1+N2+N3),1))
		matlab = matlab.astype('float32')
		imgs_test[:N1,:,:,:]=deformed
		imgs_test[N1:N1+N2,:,:,:]=matlab
		mydata = dataProcess(self.img_rows, self.img_cols,self.lab_rows, self.lab_cols)
		temp = mydata.create_label_test_data(N3)
		imgs_test[N1+N2:,:,:,:]=temp
		imgs_labels[N1:,0]=1

		#imgs_test = mydata.load_test_data()
		return imgs_test,imgs_labels

	def get_unet(self):

		inputs = Input((self.img_rows, self.img_cols,1))
		'''
		num1=64
		conv1 = Conv2D(num1, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(inputs)
		conv1 = Conv2D(num1, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv1)
		#cn1=88
		#crop1 = Cropping2D(cropping=((cn1,cn1),(cn1,cn1)))(conv1)
		pool1 = MaxPooling2D(pool_size=(2, 2))(conv1)
		'''
		num2=64
		conv2 = Conv2D(num2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(inputs)
		conv2 = Conv2D(num2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv2)
		#cn2=40
		#crop2 = Cropping2D(cropping=((cn2,cn2),(cn2,cn2)))(conv2)
		pool2 = MaxPooling2D(pool_size=(2, 2))(conv2)

		num3=128
		conv3 = Conv2D(num3, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool2)
		conv3 = Conv2D(num3, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv3)
		#cn3=16
		#crop3 = Cropping2D(cropping=((cn3,cn3),(cn3,cn3)))(conv3)
		pool3 = MaxPooling2D(pool_size=(2, 2))(conv3)

		num4=256
		conv4 = Conv2D(num4, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool3)
		conv4 = Conv2D(num4, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv4)
		drop4 = Dropout(0.5)(conv4)
		#cn4=4
		#crop4 = Cropping2D(cropping=((cn4,cn4),(cn4,cn4)))(drop4)
		pool4 = MaxPooling2D(pool_size=(2, 2))(drop4)

		num5=512
		conv5 = Conv2D(num5, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(pool4)
		conv5 = Conv2D(num5, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv5)
		drop5 = Dropout(0.5)(conv5)

		up6 = Conv2D(num4, 2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling2D(size = (2,2))(drop5))
		merge6 = concatenate([drop4,up6], axis = 3)
		conv6 = Conv2D(num4, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge6)
		conv6 = Conv2D(num4, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv6)

		up7 = Conv2D(num3, 2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling2D(size = (2,2))(conv6))
		merge7 = concatenate([conv3,up7], axis = 3)
		conv7 = Conv2D(num3, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge7)
		conv7 = Conv2D(num3, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv7)

		up8 = Conv2D(num2, 2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling2D(size = (2,2))(conv7))
		merge8 = concatenate([conv2,up8],axis = 3)
		conv8 = Conv2D(num2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge8)
		conv8 = Conv2D(num2, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv8)

		'''
		up9 = Conv2D(num1, 2, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(UpSampling2D(size = (2,2))(conv8))
		merge9 = concatenate([conv1,up9],axis = 3)
		conv9 = Conv2D(num1, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(merge9)
		conv9 = Conv2D(num1, 3, activation = 'relu', padding = 'same', kernel_initializer = 'he_normal')(conv9)
		'''

		conv10 = Conv2D(1, 1, activation = 'sigmoid',name='bds')(conv8)
		model = Model(inputs = inputs, outputs = conv10)
		model.compile(optimizer = Adam(lr = 1e-4), loss = 'mean_absolute_error', metrics = ['accuracy'])

		return model

	def get_unet_orig(self):

		inputs = Input((self.img_rows, self.img_cols,1))
		bn_axis=-1
		num2=64
		
		
		conv2 = Conv2D(num2, 4, padding = 'same',strides=(2, 2), kernel_initializer = 'he_normal')(inputs)
		convs=[conv2]
		for i in range(7):
			num3=min(128*(2**i),512)
			conv2 = LeakyReLU(0.2)(conv2)
			conv2 = Conv2D(num3, 4, strides=(2, 2),padding = 'same', kernel_initializer = 'he_normal')(conv2)
			conv2 = BatchNormalization(axis=bn_axis)(conv2)
			convs+=[conv2]

		for i in range(6,-1,-1):
			num3=min(128*(2**i),512)
			conv2 = Activation('relu')(conv2)
			conv2 = Conv2DTranspose(num3, 4, strides=(2, 2),padding = 'same', kernel_initializer = 'he_normal')(conv2)
			conv2 = BatchNormalization(axis=bn_axis)(conv2)
			if i>3 :
				conv2 = Dropout(0.5)(conv2)
			conv2 = concatenate([conv2,convs[i]], axis = 3)
		conv2 = Activation('relu')(conv2)
		num3=64
		conv2 = Conv2DTranspose(num3, 4, strides=(2, 2),padding = 'same', kernel_initializer = 'he_normal')(conv2)
		conv10 = Conv2D(1, 1, activation = 'sigmoid')(conv2)
		model = Model(inputs = inputs, outputs = conv10)
		model.compile(optimizer = Adam(lr = 2e-4,beta_1=0.5), loss = 'mean_absolute_error', metrics = ['accuracy'])

		return model

	def get_discriminator(self,patch_size=0):
		#if patch_size:
		#	inputs = Input((self.patch_size, self.patch_size,1))
		#else:
		#	inputs = Input((self.patch_size//2, self.patch_size//2,1))
	#unet with crop(because padding = valid) 
		img_inputs = Input((self.lab_rows, self.lab_cols,1))
		bd_inputs = Input((self.lab_rows, self.lab_cols,1))
		conv = concatenate([img_inputs,bd_inputs], axis = 3)
		bn_axis=-1

		for i in range(7):
			num=min(64*(2**i),512)
			conv = Conv2D(num, 4,strides=2,padding = 'same', kernel_initializer = 'he_normal')(conv)
			conv = BatchNormalization(axis=bn_axis)(conv)
			conv = LeakyReLU(0.2)(conv)

		'''
		if patch_size:
			conv7 = Conv2D(512, 3,strides=2,padding = 'same', kernel_initializer = 'he_normal')(conv6)
			conv7 = BatchNormalization(axis=bn_axis)(conv7)
			conv7 = LeakyReLU(0.2)(conv7)
			conv_flat = Flatten()(conv7)
		else:
			conv_flat = Flatten()(conv6)
		'''
		conv_flat = Flatten()(conv)

		conv8 = Dense(1, activation='sigmoid')(conv_flat)
		
		patchGAN = Model(inputs = [img_inputs,bd_inputs], outputs = conv8)

		
		'''
		if patch_size:
			row_idx = [0,194,388]
			x=[]
			for i in [0,1]:
				for j in [0,1]:
					patch = Lambda(lambda z: z[:,row_idx[i]:row_idx[i+1],row_idx[j]:row_idx[j+1], :])(big_inputs)
					x.append(patchGAN(patch))
		else:
			row_idx = [0,97,194,291,388]
			x=[]
			for i in [0,1,2,3]:
				for j in [0,1,2,3]:
					patch = Lambda(lambda z: z[:,row_idx[i]:row_idx[i+1],row_idx[j]:row_idx[j+1], :])(big_inputs)
					x.append(patchGAN(patch))

		avg = average(x)
		model = Model(inputs = big_inputs, outputs = avg)
		'''
		patchGAN.compile(optimizer = Adam(lr = 2e-4,beta_1=0.5), loss = 'binary_crossentropy', metrics = ['accuracy'])

		return patchGAN


	def train(self,reset=0):

		print("loading data")
		#imgs_train, imgs_mask_train, imgs_test = self.load_data()
		imgs_train, imgs_mask_train= self.load_data()
		print("loading data done")
		if os.path.exists('unet.hdf5'):
			model = load_model('unet.hdf5')
			#model = self.get_unet()
			#loaded_model.load_weights('unet.hdf5')
			print("got unet from load")
		else:
			model = self.get_unet_orig()
			print("made unet from scratch")
		if reset:
			model.optimizer.set_state()
		model_checkpoint = ModelCheckpoint('unet.hdf5', monitor='val_acc',verbose=1, save_best_only=True)
		print('Fitting model...')
		lr_reducer = ReduceLROnPlateau(monitor='val_acc', factor=0.9, patience=2, min_lr=0.000001, verbose=1)
		model.fit(imgs_train, imgs_mask_train, batch_size=4, epochs=17, verbose=1,validation_split=0.1, shuffle=True, callbacks=[lr_reducer,model_checkpoint])


	def train_D(self):

		print("loading data")
		#imgs_train, imgs_mask_train, imgs_test = self.load_data()
		imgs_train, imgs_mask_train= self.load_deform_data()
		print("loading data done")
		if os.path.exists('discern.hdf5'):
			model = load_model('discern.hdf5')
			print("got discern from load")
		else:
			model = self.get_discriminator()
			print("made discern from scratch")


		
		model_checkpoint = ModelCheckpoint('discern.hdf5', monitor='val_acc',verbose=1, save_best_only=False)
		print('Fitting model...')
		#lr_reducer = ReduceLROnPlateau(monitor='val_acc', factor=0.9, patience=4, min_lr=0.000001, verbose=1)
		model.fit(imgs_train, imgs_mask_train, batch_size=2, epochs=4, verbose=1,validation_split=0, shuffle=True, callbacks=[model_checkpoint])	#np.save('../results/imgs_mask_test.npy', imgs_mask_test)

	def train_GAN(self,mode=1,batch_size=4,epochs=20000,save_interval=10004):
		logging.basicConfig(filename='tem'+str(mode)+'.log',level=logging.INFO)
		logger = logging.getLogger(__name__)
		logger.info("loading data")
		mydata = dataProcess(self.img_rows, self.img_cols,self.lab_rows, self.lab_cols)
		imgs_train, imgs_mask_train= self.load_data()
		N=imgs_train.shape[0]
		#matlab=np.load('data/samples/matlab.npy')
		#matlab = matlab.astype('float32')
		#Nm=matlab.shape[0]
		#N=imgs_test.shape[0]
		#imgs_labels=np.ones((N,1))
		

		logger.info("loading data done")
		if os.path.exists('discern'+str(mode)+'.hdf5'):
			D = load_model('discern'+str(mode)+'.hdf5')
			logger.info("got discern from load mode")
		else:
			if mode==2:
				D = self.get_discriminator(1)
				logger.info("made discern from scratch big")
			elif mode==4:
				D = load_model('discern.hdf5')
				logger.info("got discern from load")
			else:
				D = self.get_discriminator()
				logger.info("made discern from scratch")
			

		D.trainable = False
		if os.path.exists('unet'+str(mode)+'.hdf5'):
			G = load_model('unet'+str(mode)+'.hdf5')
			logger.info("got unet mode")
		else:
			if mode==4:
				G = load_model('unet.hdf5')
				logger.info("got unet pretrain")
			else:
				G = self.get_unet_orig()
				logger.info("made unet from scratch")


		inputs = Input((self.img_rows, self.img_cols,1))
		generated_image = G(inputs)
		x=D([inputs,generated_image])
		if mode==3:
			lmda=1/2.
		elif mode==5:
			lmda=0.15
		else:
			lmda=1/10.
		#def joint_loss(y_true, y_pred):
		#	return 2*(losses.binary_crossentropy(y_true[1], y_pred[1])+lmda*losses.mean_absolute_error(y_true[0], y_pred[0]))

		model = Model(inputs = inputs, outputs = [generated_image,x])
		model.compile(optimizer = Adam(lr = 2e-4,beta_1=0.5), loss=['mean_absolute_error', 'binary_crossentropy'],
			loss_weights=[1., lmda],metrics = ['accuracy'])
		#model = Model(inputs = inputs, outputs = x)
		#model.compile(optimizer = Adam(lr = 1e-4), loss = 'binary_crossentropy', metrics = ['accuracy'])	
		for epoch in range(epochs):
			#train discrimator
			sample=np.random.choice(N,size=batch_size)
			imgs_train_batch=imgs_train[sample,:,:,:]
			imgs_mask_train_batch=imgs_mask_train[sample,:,:,:]
			d_loss_real = D.train_on_batch([imgs_train_batch,imgs_mask_train_batch], np.ones((batch_size,1)))
			
			sample=np.random.choice(N,size=batch_size)
			imgs_train_batch=imgs_train[sample,:,:,:]
			gen_imgs = G.predict(imgs_train_batch,batch_size=1)

			d_loss_fake = D.train_on_batch([imgs_train_batch,gen_imgs], np.zeros((batch_size,1)))
			d_loss = 0.5 * np.add(d_loss_real, d_loss_fake)
			#Train Generator
			num=2
			for twice in range(num):
				sample=np.random.choice(N,size=batch_size)
				imgs_train_batch=imgs_train[sample,:,:,:]
				imgs_mask_train_batch=imgs_mask_train[sample,:,:,:]
				if twice==0:
					g_loss=model.train_on_batch(imgs_train_batch, [imgs_mask_train_batch,np.ones((batch_size,1))])
				else:
					g_loss=np.add(g_loss,model.train_on_batch(imgs_train_batch, [imgs_mask_train_batch,np.ones((batch_size,1))]))
				
			if (epoch+1) % 100 == 0:
				logger.info("%d [D loss: %f acc: %.2f%%] [G loss: %f match acc: %.2f%% discrimator acc: %.2f%%]" % ((epoch+1), d_loss[0], d_loss[1]*100, g_loss[0]/num, 100*g_loss[3]/num,100*g_loss[4]/num))
			if (epoch+1) % save_interval == 0:
				G.save('unet'+str(mode)+'.hdf5')
				D.save('discern'+str(mode)+'.hdf5')
		
		G.save('unet'+str(mode)+'.hdf5')
		D.save('discern'+str(mode)+'.hdf5')

	def stitch(self,image,pieces,name,overlap):
	    [m,n]=image.shape
	    [Z1,tar_rows,tar_cols,Z2]=pieces.shape
	    numr=m//(tar_rows-overlap)+1
	    numc=n//(tar_cols-overlap)+1
	    boundary=np.zeros((numr*(tar_rows-overlap)+overlap,numc*(tar_cols-overlap)+overlap))
	    z = 0
	    for i in range(numr):
	        for j in range(numc):
	            colstart=j*(tar_cols-overlap)
	            rowstart=i*(tar_rows-overlap)
	            boundary[rowstart:rowstart+tar_rows,colstart:colstart+tar_cols]=pieces[z,:,:,0]
	            z += 1
	    image=np.expand_dims(image,2)
	    boundary=boundary[:m,:n]
	    np.save('results/'+name+'boundary.npy',boundary)
	    boundary=np.expand_dims(boundary,2)
	    
	    img = array_to_img(image)
	    img.save("results/"+name+"image.jpg")
	    img = array_to_img(1-boundary)
	    img.save("results/"+name+"boundary.jpg")

	def test(self,num=1,name='',mode='0',overlap=20):
		if mode=='0':
			model = load_model('unet.hdf5')
		else:
			model = load_model('unet'+str(mode)+'.hdf5')
		imgs_test,image_orig= self.load_test(num)
		imgs_mask_test = model.predict(imgs_test, batch_size=1, verbose=1)
		self.stitch(image_orig,imgs_mask_test,name,overlap)


	def test_deform(self,num=2000):
		model = load_model('unet.hdf5')
		imgs_test= self.load_deform_test(num//2)
		imgs_train, void= self.load_data()
		N=imgs_train.shape[0]
		sample=np.random.choice(N,size=num//2)
		imgs_train=imgs_train[sample,:,:,:]
		imgs_mask_test = model.predict(imgs_test, batch_size=1, verbose=1)
		imgs_mask_train = model.predict(imgs_train, batch_size=1, verbose=1)
		combined=np.concatenate((imgs_mask_test, imgs_mask_train), axis=0)
		np.save('data/samples/deformed',combined)

	def save_img(self):

		print("array to image")
		imgs = np.load('/results/imgs_mask_test.npy')
		for i in range(imgs.shape[0]):
			img = imgs[i]
			img = array_to_img(img)
			img.save("results/%d.jpg"%(i))




if __name__ == '__main__':
	if 'augment' in sys.argv:
		aug = myAugmentation()
		aug.Augmentation(5000)
	if 'discerndata' in sys.argv:
		myunet = myUnet()
		myunet.test_deform()
	if 'train' in sys.argv:
		myunet = myUnet()
		myunet.train()
	if 'traindefrom' in sys.argv:
		myunet = myUnet()
		myunet.train_D()
	if 'traingan' in sys.argv:
		myunet = myUnet()
		mode=int(sys.argv[-1])
		myunet.train_GAN(mode)

	if 'test' in sys.argv:
		mode=sys.argv[-3]
		num=int(sys.argv[-2])
		name=sys.argv[-1]
		myunet = myUnet()
		myunet.test(num,name,mode)
	#myunet.save_img()








